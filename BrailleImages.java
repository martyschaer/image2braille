import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.PrintWriter;

/**
 * Larger images may be difficult to view in text later.
 * Scale them down beforehand in your favourite picture editor.
 */
public class BrailleImages {
    private static final String INPUT_PATH = "your/path/goes/here";
    private static final int BRAILLE_HEIGHT = 4;
    private static final int BRAILLE_WIDTH = 2;
    private static final int BLACK = 0xff000000;
    private static final int WHITE = 0xffffffff;
    private static final int LAST_BYTE = 0x000000ff;
    private static final int BRAILLE_BLANK = 0x2800;

    private static int outHeight;
    private static int outWidth;

    public static void main(String[] args) throws Exception {
        BufferedImage img = ImageIO.read(new File(INPUT_PATH));
        if(img.getHeight() % BRAILLE_HEIGHT != 0 || img.getWidth() % BRAILLE_WIDTH != 0){
            throw new IllegalArgumentException("Image must have height divisible by 4 and width divisible by 2");
        }
        outHeight = img.getHeight() / BRAILLE_HEIGHT;
        outWidth = img.getWidth() / BRAILLE_WIDTH;
        img = convertImageToBlackAndWhite(img);
        try(PrintWriter out = new PrintWriter("output.txt")) {
            out.print(convertToBraille(img));
        }
    }

    private static String convertToBraille(BufferedImage in){
        StringBuilder sb = new StringBuilder();
        for(int y = 0; y < outHeight; y++){
            for(int x = 0; x < outWidth; x++){
                sb.append(Character.toChars(convertBlockToBrailleCharacter(x*BRAILLE_WIDTH, y*BRAILLE_HEIGHT, in)));
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /** Goes through a 2x4 braille block of pixels
     *  Numbered like so:
     *  [1][4]
     *  [2][5]
     *  [3][6]
     *  [7][8]
     *  and adds a progressively more shifted single bit number, as explained here:
     *  https://en.wikipedia.org/wiki/Braille_Patterns#Identifying,_naming_and_ordering
     */
    private static int convertBlockToBrailleCharacter(int x, int y, BufferedImage img){
        int ch = BRAILLE_BLANK;
        int position = 0x1;
        for(int xa = 0; xa <= 1; xa++){
            for(int ya = 0; ya <= 2; ya++){
                ch = convertPixel(img.getRGB(x+xa, y+ya), ch, position);
                position <<= 1;
            }
        }
        //didn't fit into a loop neatly
        ch = convertPixel(img.getRGB(x+0, y+3), ch, 0x40); //block 7
        ch = convertPixel(img.getRGB(x+1, y+3), ch, 0x80); //block 8
        return ch;
    }

    private static int convertPixel(int argb, int ch, int toAdd){
        return argb == BLACK ? ch : ch+toAdd;
    }

    private static BufferedImage convertImageToBlackAndWhite(BufferedImage input) {
        BufferedImage output = new BufferedImage(input.getWidth(), input.getHeight(), BufferedImage.TYPE_INT_ARGB);
        for (int x = 0; x < input.getWidth(); x++) {
            for (int y = 0; y < input.getHeight(); y++) {
                int argb = input.getRGB(x, y);
                output.setRGB(x, y, getLuminance(argb) < 0x80 ? BLACK : WHITE);
            }
        }
        return output;
    }

    private static int getLuminance(int argb){
        // values brightness (green) higher
        int sum = getRed(argb) + 4 * getGreen(argb) + getBlue(argb);
        int avg = sum / 6;
        return (avg * 0xff) / getAlpha(argb);
    }

    private static int getAlpha(int argb) {
        return (argb >> 24 & LAST_BYTE);
    }

    private static int getRed(int argb) {
        return (argb >> 16 & LAST_BYTE);
    }

    private static int getGreen(int argb) {
        return (argb >> 8 & LAST_BYTE);
    }

    private static int getBlue(int argb) {
        return argb & LAST_BYTE;
    }

}